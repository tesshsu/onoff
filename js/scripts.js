/**
 * (c) 2016 數位甜心
 *
 */
(function(){

	// Init global DOM elements, functions and arrays
    window.app 			                   = {el : {}, fn : {}};
    app.el['window']                   = $(window);
    app.el['document']                 = $(document);
    app.el['back-to-top']              = $('.back-to-top');
    app.el['html-body']                = $('html,body');	
    app.el['loader']                   = $('#loader');
    app.el['mask']                     = $('#mask');

	app.fn.screenSize = function() {
		var size, width = app.el['window'].width();
		if(width < 320) size = "Not supported";
		else if(width < 480) size = "Mobile portrait";
		else if(width < 768) size = "Mobile landscape";
		else if(width < 960) size = "Tablet";
		else size = "Desktop";
		if (width < 768){$('.animated').removeClass('animated').removeClass('hiding');}
		// $('#screen').html( size + ' - ' + width );
		// console.log( size, width );
	};	

	$(function() {	
    //Preloader
    app.el['loader'].delay(700).fadeOut();
    app.el['mask'].delay(1200).fadeOut("slow");    

    // Resized based on screen size
    app.el['window'].resize(function() {
      app.fn.screenSize();
    });	
    //accordion	
    $(".accordion-toggle").click(function(){
      $(".accordion-body").collapse('show');
    });
     $(".panel-heading").addClass("collapsed");
    //slider
    $('.carousel').carousel({
      interval: 2000
    })
    // fade in .back-to-top
    $(window).scroll(function () {
      if ($(this).scrollTop() > 500) {
        app.el['back-to-top'].fadeIn();
      } else {
        app.el['back-to-top'].fadeOut();
      }
    });

    // scroll body to 0px on click
    app.el['back-to-top'].click(function () {
      app.el['html-body'].animate({
        scrollTop: 0
      }, 1500);
      return false;
    });

    $('#form-order').submit(function(){
    	// ajax inesrt order then submit form to payment
      //show error sign if input val empty
      var allInput = $('#form-order').find('input');
      $.each(allInput,function(index, input){
        if($(this).val().length == 0) {
          $(this).focus();
          $('.errorsign-icon').fadeIn();
          return false;
        }else if( $(this).val().length > 0) {
          $('.errorsign-icon').hide();
          return true;
        }
      });    	
      $.ajax({
        method: "POST",
        url: "orders/create",
        dataType : "json",
        data: $('#form-order').serialize()
      }).done(function( feedback ) {
        if(feedback.success){
          $('#form-payment input[name="Email"]').val($('#form-order input[name="order_email"]').val());
          $('#form-payment input[name="CheckValue"]').val(feedback.CheckValue);
          $('#form-payment input[name="TimeStamp"]').val(feedback.TimeStamp);
          $('#form-payment input[name="MerchantOrderNo"]').val(feedback.MerchantOrderNo);
          $('#form-payment input[name="Amt"]').val(feedback.Amt);
          //if(confirm('前往付款'))
          //{
            $('#form-payment').submit();
          //}
        }else{
          alert(feedback.msg);
        }
      });
      return false;      
    });

    $('#mobileheader').html($('#header').html());

    function heroInit() {
        var hero        = jQuery('#hero'),
            winHeight   = jQuery(window).height(),
            heroHeight  = winHeight;
          
            hero.css({height: heroHeight+"px"});
      };
      
    jQuery(window).on("resize", heroInit);
    jQuery(document).on("ready", heroInit);
    
    $('.navigation-bar').onePageNav({
        currentClass: 'active',
        changeHash: true,
        scrollSpeed: 750,
        scrollThreshold: 0.5,
        easing: 'swing'
    });
    $('.btn-scroll').onePageNav({
        currentClass: 'active',
        changeHash: true,
        scrollSpeed: 750,
        scrollThreshold: 0.5,
        easing: 'swing'
    });
    
    $('.animated').appear(function(){
      var element = $(this);
      var animation = element.data('animation');
      var animationDelay = element.data('delay');
      if (animationDelay) {
        setTimeout(function(){
          element.addClass( animation + " visible" );
          element.removeClass('hiding');
          if (element.hasClass('counter')) {
            element.find('.value').countTo();
          }
        }, animationDelay);
      }else {
        element.addClass( animation + " visible" );
        element.removeClass('hiding');
        if (element.hasClass('counter')) {
          element.find('.value').countTo();
        }
      }    
    },{accY: -150});
    
    $('#header').waypoint('sticky', {
        wrapper: '<div class="sticky-wrapper" />',
        stuckClass: 'sticky'
    }); 
    
	});
	
})();